In the API we feature a cleanerThan value which reports what percentage the test
is cleaner than. To improve performance, we hardcoded the percentiles and the
emissions associated and simply lookup where it fits, rather than requesting the
live data from the database. The site effect of this is that the values become
out of date as time goes on, thus we need to generate a new json file
periodically that contains the most up-to-date data.

What follows is the process for generating the files

1. Generate a new SQL table with the most recent tests for each unique url, for
the time period you are interested in.

This can be achieved with the following SQL statements

---

DROP TABLE IF EXISTS results;

CREATE TABLE `results` (
  `co2` decimal(18,16) DEFAULT NULL
);

INSERT into results
SELECT co2
FROM record
WHERE id IN (
    SELECT MAX(id)
    FROM record
    GROUP BY url
) AND timestamp > '2020-01-01 00:00:00'
ORDER BY co2 ASC;

---

2. Export the results table to a csv in the rankings folder.

3. Update index.php to reference the latest file you just created
Change line 3 to the filename of the csv you just created

4. Run index.php

This will generate a json file with 100 points that associate percentiles to
emissions

5. Update $percentiles in the function cleanerThan on line 48 of includes/traits/helpers.php

6. Commit the changes to git
